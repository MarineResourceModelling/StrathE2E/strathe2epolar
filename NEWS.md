# StrathE2EPolar 2.1.0

# Version 2.1.0 includes some minor modifications
  and bug fixes mainly relating to the addition of a parameter
  to control the wellmixedness of the inshore zone.

# Version 2.0.0 includes a change to the handling of prey 
  selection by omnivorous zooplankton, and some minor modifications
  and bug fixes.

  * In 1.0.0 a method for zooplankton switching between
    preference for phytoplankton and detritus was implemented.
    Subsequent investigation showed that this was an
    un-necessary complication. So, in 2.0.0 preference coefficients
    have been reverted to the original fixed values. This required
    reoptimization of the ecology parameters for the Barents Sea
    model included in the package.

  * Additional data on ice algae biomass included in the target
    dataset for optimization.

  * Modifications to the Monte Carlo function to take account
    of the removal of two parameters due to the zooplankton
    feeding preference (above). In addition, all parameters
    which are fixed and not subject to optimization are now
    included in teh MC process. Improvement of the graphical
    display for Monte Carlo parameter intervals.

  * Version 2.0.0 was used to generate outputs for the revised 
    publication: Heath, M.R., Benkort, D., Brierley, A.S., 
    Daewel, U., Laverick, J.H., Proud R. and Speirs, D.C. Ecosystem 
    approach to harvesting in the Arctic: walking the 
    tightrope between exploitation and conservation in the Barents
    Sea. Ambio, Special issue: Changing Arctic Ocean (accepted
    August 2021).

* Version 1.0.0 was developed from StrathE2E2 version 3.3.0.
  The latter is a food web/ecosystem model for temperate shelf
  seas. StrathE2EPolar is designed for polar shelf sea which are
  affected by sea ice. The development includes:

  * Addition of time-varying ice extent, cover and thickness,
    and air temperature data to the list of driving data.

  * Addition of state variables to represent nutrients, detritus
    and algae in the ice, and their connections to the rest of
    the food web.

  * Addition of state variables to represent the dynamics of
    maritime mammals (polar bears).

  * Addition of representation of ice-depenedency of feeding
    and active migrations by top predators.

  * Version 1.0.0 contributes to the submitted publication:
    Heath, M.R., Benkort, D., Brierley, A.S., Daewel, U.,
    Laverick, J.H., Proud R. and Speirs, D.C. Ecosystem 
    approach to harvesting the changing Arctic: walking the 
    tightrope between exploitation and conservation. Ambio, 
    Special issue: Changing Arctic Ocean (submitted April 2021).
