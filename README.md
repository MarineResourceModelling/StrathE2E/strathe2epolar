# StrathE2EPolar
R package containing the Strath End-to-end ecosystem model for polar seas.

## General Requirements

StrathE2EPolar runs on the following platforms:

* Mac OS X
* Linux (64-bit)
* Windows (64-bit, Windows 10 recommended)

with the following minimum runtime:

* R 3.6 or later
* R packages deSolve, NetIndices

## Installation

There is a source package for Linux and binary packages for Windows 10 and Mac OS X in our [CRAN](https://cran.r-project.org/) compatible [R package repository](https://marineresourcemodelling.gitlab.io/sran/index.html).

To install StrathE2EPolar, use install.packages() function and specify our repository:
```
install.packages("StrathE2EPolar", repos="https://marineresourcemodelling.gitlab.io/sran")
```

which will install either the source or binary package (depends on your OS).

## Quick Start

To run the model:

```
library(StrathE2EPolar)
model<-e2ep_read("Barents_Sea","2011-2019")
results<-e2ep_run(model, nyears=5)
e2ep_plot_ts(model, results)
```

Within R, type *help.start()* and navigate to the package documentation for various user and technical guides,

## Building from source

The source package can be built on all platforms, and is the only way to install on Linux.

Download latest version from [StrathE2E GitLab repository](https://gitlab.com/MarineResourceModelling/StrathE2E/StrathE2EPolar) - use the 'download' button.

In addition to the standard requirements, some additional R packages are required to build the package documentation (vignettes):

  * knitr
  * rmarkdown

For each platform there are some additional software requirements - see the individual plaform build sections below.

### Linux build

Full requirements for Linux build:

| Requirement	| URL/Comment					|
|---------------|-----------------------------------------------|
| R		| https://cran.r-project.org/bin/windows/	|
| R packages	| deSolve, NetIndices, knitr, rmarkdown		|
| pandoc	| https://pandoc.org/				|
| Texlive	| https://www.tug.org/texlive/			|

Usually Linux distributions will have everything either already installed, or available for install via the system package manager.

At a terminal prompt, try:

> R CMD INSTALL StrathE2EPolar-master.tar.gz

The package will be installed in your local R library folder (often ~/R).

### Windows build

Full requirements for Windows build:

Full requirements for Windows build:

| Requirement	| URL/Comment					|
|---------------|-----------------------------------------------|
| R		| https://cran.r-project.org/bin/windows/	|
| R packages	| deSolve, NetIndices, knitr, rmarkdown		|
| Rtools	| https://cran.r-project.org/bin/windows/Rtools	|
| pandoc	| https://pandoc.org/				|
| MikTeX	| https://miktex.org/				|
| pdfpages      | https://ctan.org/pkg/pdfpages                 |

Install *Rtools* as administrator and make sure the box to *Add rtools to the system PATH* is checked.

Add the *R* bin folder to the system PATH as well. Go to *Control Panel*->*System and Security*->*System* then select *Advanced system settings*. Click on the *Environment Variables* button edit the *PATH* variable, adding the path to the *R* bin folder, something like:
```
> C:\Program Files\R\R-3.6.1\bin
```

If you upgrade *R* at a later date you must remember to update this *PATH* setting as well.

In a *cmdtool* navigate to working folder where the unpacked source code resides in a sub-folder.

Start R and run:
```
> library(devtools)
> document("StrathE2EPolar")
```

Then quit R and on command line:
```
> R CMD build StrathE2EPolar
> R CMD INSTALL --build StrathE2EPolar_2.1.0.tar.gz
```

This will build the binary package *StrathE2EPolar_2.1.0.zip*


### MacOS X build

Full requirements for MacOS build:

| Requirement	| URL/Comment					|
|---------------|-----------------------------------------------|
| R		| https://cran.r-project.org/bin/macosx		|
| R packages:	| deSolve, NetIndices, knitr, rmarkdown		|
| X11 Quartz	| https://www.xquartz.org/			|
| pandoc	| https://pandoc.org/				|
| MacTeX	| https://www.tug.org/mactex			|

make sure to restart any *terminal* applications after installing these packages.

Within a *Terminal*, unpack the downloaded source package and install/build:

	tar zxvf StrathE2EPolar-master.tar.gz
	R CMD INSTALL --build StrathE2EPolar-master

If the command line developer tools are missing you should accept the offer of installing them.

A successful build will install the package in R, and there will also be a binary package file *StrathE2EPolar-2.1.0.tgz* created.

